"""
#TIPOS DE DATOS
numero_entero=5
print("el tipo de dato es: ",type(numero_entero))
numero_flotante=5.4
print("el tipo de dato es: ",type(numero_flotante))
cadena = "hola"
print(cadena + str(numero_flotante))
lista = [1, 1.0,False, [1,2,3], "hola"]
print(lista[3])
diccionario = {"numero_1":1,"lista":[1,2,3], "bool":False, "Dic":{"1":1}}
print(diccionario["Dic"])
tupla = (1,2,3)
print(tupla)
"""
"""
#OPERADORES ARITMETICOS
print("3+3=",3+3)
print("5-6=",5-6)
print("3*5=",3*5)
print("6/9=",6/9)
print("6%9=",6%9)
print("6//9=",6//9)
print("2**3=",2**3)
"""
"""
#OPERADORES DE COMPARACIÓN
print("3>3?",3>3)
print("3==3?",3==3)
print("2<=1?",2<=1)
print("5>2?",5>2)
"""

#OPERADORES LOGICOS
"""
print("3>3 or 3>2 or 5>10?", 3>3 or 3>2 or 5>10)
print("3>3 and 3>2 and 5>10?", 3>3 and 3>2 and 5>10)
print("not 3>3?",not 3>3)
print("3>3 and 3>2 or 5>10?", (3>3 and 3>2) or 10>5)
"""
#IF ELSE (BIFURCACIONES)
"""
if(5>3):
    print("esta si se cumple")
    if(5>10):
        print("esta tambien")
    else:
        print("pero esta no")
        if(8>5):
            print("pero esta si")
else:
    print("no se cumple la condicion")

if(3>3):
    print("tres mayor a tres")
elif(5>10):
    print("cinco mayor a tres")
elif(10>4):
    print("diez mayor a cuatro")
elif(12>4):
    print("doce mayor a cuatro")
else:
    print("nada es verdad")
"""
"""
#REASIGNACION DE VARIABLES
numero= 10
numero+=10#numero +10
print(numero)
"""
"""
#CICLO WHILE
contador=0

while(contador<20):
    print(contador)
    if(contador>8 and contador < 10):
        print("contador es 9")
        while(contador<15):
            print("estoy en while 2")
            contador+=1
    contador+=1 
"""
#CICLO FOR
"""
for i in range(0,20):
    print(i)

lista = [1,4.5,["h","o","l",1], {"i":0}]

for elem in lista:
    print(elem)

diccionario ={"uno":"Enero","dos":"Febrero","tres":"Marzo"}
for elem in diccionario: #toma el valor de las key
    print(elem)
print("   ")
for val in diccionario.values():
    print(val)
print(" ")

for llave,valor in diccionario.items():
    print(llave,valor)
"""
"""
#Sentencias Break y continue
cont=0
while(cont < 20):
    cont+=1
    if(cont==7):
        print("break")
        break
    if(cont==5):
        print("continue")
        continue
    print(cont)
"""
"""
#FUNCIONES
import mis_funciones
lista = [1,2,3,4,5]
mi_media= mis_funciones.media(lista)
print(mi_media)
"""
#manejo de excepciones (try-except)
try:
    numero = int(input("ingrese un numero entero: "))
    numero=numero+1
    print(numero)
except:
    print("formato no valido")
print("saliendo del programa...")